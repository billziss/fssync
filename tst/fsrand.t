#!/bin/bash

ProgDir=$(cd $(dirname "$0") && pwd)
ProgNam=$(basename "$0")
fssync="$ProgDir/../fssync.py"

opts=()
while getopts aws:c:d: opt; do
    case "$opt" in
    a) altrand=y ;;                     # alternative randomization algorithm
    w) swap=y ;;                        # swap fssync.py file system arguments
    s) seed="$OPTARG" ;;
    c|d) opts=("${opts[@]}" "-$opt" "$OPTARG")
    esac
done
shift $(($OPTIND - 1))
: ${seed=$(date +%s)}
cycles=${1-1}; shift
verbose=${1-1000000000}; shift
echo "cycles=$cycles seed=$seed"

tmp="/tmp/$ProgNam.$$"
trap "rm -rf \"$tmp\"" EXIT
mkdir "$tmp"/{,s,l,o}
for i in $(seq 1 $cycles); do
    echo "cycle $i"
    [[ $i -ge $verbose ]] && v=-vv || v=
    fsrand.py "${opts[@]}" -s $seed "$tmp/l" >/dev/null; (( seed += 1 ))
    fsrand.py "${opts[@]}" -s $seed "$tmp/o" >/dev/null; (( seed += 1 ))
    if [[ -n "$altrand" ]]; then
        fsrand.py "${opts[@]}" -s $seed "$tmp/l" >/dev/null
        fsrand.py "${opts[@]}" -s $seed "$tmp/o" >/dev/null
        (( seed += 1 ))
    fi
    if [[ -z "$swap" ]]; then
        "$fssync" $v -i "cycle$i" "$tmp/s" "$tmp/l" "$tmp/o" || \
            { ec=$?; echo "fssync.py exited with exitcode $ec" 1>&2; exit $ec; }
    else
        "$fssync" $v -i "cycle$i" "$tmp/s" "$tmp/o" "$tmp/l" || \
            { ec=$?; echo "fssync.py exited with exitcode $ec" 1>&2; exit $ec; }
    fi
    cmptree.py "$tmp/l" "$tmp/o" || exit
done
