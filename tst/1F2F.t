#!/bin/bash

ProgDir=$(cd $(dirname "$0") && pwd)
ProgNam=$(basename "$0")
fssync="$ProgDir/../fssync.py"

cycle1()
{
    mkdir "$tmp"/l/d
    echo cycle1 > "$tmp"/l/d/f
}
cycle2()
{
    echo cycle2 > "$tmp"/l/d/f
    rm "$tmp"/o/d/f
    rmdir "$tmp"/o/d
    echo dee-cycle2 > "$tmp"/o/d
}

tmp="/tmp/$ProgNam.$$"
trap "rm -rf \"$tmp\"" EXIT

rm -rf \"$tmp\"
mkdir "$tmp"/{,s,l,o}
echo "1F: cycle 1"
cycle1
"$fssync" -i cycle1 "$tmp/s" "$tmp/l" "$tmp/o" || \
    { ec=$?; echo "fssync.py exited with exitcode $ec" 1>&2; exit $ec; }
cmptree.py "$tmp/l" "$tmp/o" || exit
echo "1F: cycle 2"
cycle2
"$fssync" -i cycle1 "$tmp/s" "$tmp/l" "$tmp/o" || \
    { ec=$?; echo "fssync.py exited with exitcode $ec" 1>&2; exit $ec; }
cmptree.py "$tmp/l" "$tmp/o" || exit

rm -rf "$tmp"
mkdir "$tmp"/{,s,l,o}
echo "2F: cycle 1"
cycle1
"$fssync" -i cycle1 "$tmp/s" "$tmp/o" "$tmp/l" || \
    { ec=$?; echo "fssync.py exited with exitcode $ec" 1>&2; exit $ec; }
cmptree.py "$tmp/l" "$tmp/o" || exit
echo "2F: cycle 2"
cycle2
"$fssync" -i cycle1 "$tmp/s" "$tmp/o" "$tmp/l" || \
    { ec=$?; echo "fssync.py exited with exitcode $ec" 1>&2; exit $ec; }
cmptree.py "$tmp/l" "$tmp/o" || exit
