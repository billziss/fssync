#!/usr/bin/env python
# coding=UTF-8

# Copyright (c) 2015, Bill Zissimopoulos. All rights reserved.
#
# Redistribution  and use  in source  and  binary forms,  with or  without
# modification, are  permitted provided that the  following conditions are
# met:
#
# 1.  Redistributions  of source  code  must  retain the  above  copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions  in binary  form must  reproduce the  above copyright
# notice,  this list  of conditions  and the  following disclaimer  in the
# documentation and/or other materials provided with the distribution.
#
# 3.  Neither the  name  of the  copyright  holder nor  the  names of  its
# contributors may  be used  to endorse or  promote products  derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY  THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND  ANY EXPRESS OR  IMPLIED WARRANTIES, INCLUDING, BUT  NOT LIMITED
# TO,  THE  IMPLIED  WARRANTIES  OF  MERCHANTABILITY  AND  FITNESS  FOR  A
# PARTICULAR  PURPOSE ARE  DISCLAIMED.  IN NO  EVENT  SHALL THE  COPYRIGHT
# HOLDER OR CONTRIBUTORS  BE LIABLE FOR ANY  DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL,  EXEMPLARY,  OR  CONSEQUENTIAL   DAMAGES  (INCLUDING,  BUT  NOT
# LIMITED TO,  PROCUREMENT OF SUBSTITUTE  GOODS OR SERVICES; LOSS  OF USE,
# DATA, OR  PROFITS; OR BUSINESS  INTERRUPTION) HOWEVER CAUSED AND  ON ANY
# THEORY  OF LIABILITY,  WHETHER IN  CONTRACT, STRICT  LIABILITY, OR  TORT
# (INCLUDING NEGLIGENCE  OR OTHERWISE) ARISING IN  ANY WAY OUT OF  THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# BASED ON THE PAPER:
#
#     “AAFS”: An Algebraic Approach to File Synchronization, Norman Ramsey, Elod Csirmaz
#
# START WITH THE LAW SET FROM “AAFS”:
#
#     Commuting or approximating pairs
#     1.    edit(π,X); edit(π/π′,Y) ≡ edit(π/π′,Y); edit(π,X)
#     2.    edit(π/π′,Y); edit(π,X) ≡ edit(π,X); edit(π/π′,Y)
#     3D⊑.  edit(π,Dir(m)); create(π/π′,Y) ⊒ create(π/π′,Y); edit(π, Dir(m))
#     4D⊑.  create(π/π′,Y); edit(π,Dir(m)) ⊑ edit(π, Dir(m)); create(π/π′,Y)
#     5D.   edit(π, Dir(m)); remove(π/π′) ≡ remove(π/π′); edit(π, Dir(m))
#     6D.   remove(π/π′); edit(π, Dir(m)) ≡ edit(π, Dir(m)); remove(π/π′)
#     7.    edit(π,X); edit(φ,Y) ≡ edit(φ,Y); edit(π,X)
#     8.    edit(π,X); create(φ,Y) ≡ create(φ,Y); edit(π,X)
#     9.    edit(π,X); remove(φ) ≡ remove(φ); edit(π, X)
#     10.   create(φ,Y); edit(π,X) ≡ edit(π,X); create(φ,Y)
#     11.   create(π,X); create(φ,Y) ≡ create(φ,Y); create(π,X)
#     12.   create(π,X); remove(φ) ≡ remove(φ); create(π,X)
#     13.   remove(φ); edit(π,X) ≡ edit(π,X); remove(φ)
#     14.   remove(φ); create(π,X) ≡ create(π,X); remove(φ)
#     15.   remove(π); remove(φ) ≡ remove(φ); remove(π)
#
#     Incorrect pairs
#     3F.   edit(π, File(m, x)); create(π/π′,Y) ≡ break
#     4F.   create(π/π′,Y); edit(π, File(m, x)) ≡ break
#     5F.   edit(π, File(m, x)); remove(π/π′) ≡ break
#     16.   edit(π, X); create(π,Y) ≡ break
#     17.   edit(π/π′,X); create(π,Y) ≡ break
#     18.   edit(π/π′,X); remove(π) ≡ break
#     19.   create(π,X); edit(π/π′,Y) ≡ break
#     20.   create(π,X); create(π,Y ) ≡ break
#     21.   create(π/π′,X); create(π,Y) ≡ break
#     22.   create(π,X); remove(π/π′) ≡ break
#     23.   create(π/π′,X); remove(π) ≡ break
#     24.   remove(π); edit(π,X) ≡ break
#     25.   remove(π); edit(π/π′,X) ≡ break
#     26.   remove(π); create(π/π′,X) ≡ break
#     27.   remove(π/π′); create(π,X) ≡ break
#     28.   remove(π); remove(π) ≡ break
#     29.   remove(π); remove(π/π′) ≡ break
#
#     Simplifying laws
#     30⊑.  edit(π,X); edit(π,Y) ⊑ edit(π,Y)
#     31.   edit(π,X); remove(π) ≡ remove(π)
#     32.   create(π,X); edit(π,Y) ≡ create(π,Y)
#     33⊑.  create(π,X); remove(π) ⊑ skip
#     34⊑.  remove(π); create(π,X) ⊑ edit(π,X)
#
#     Break is idempotent
#     37.   break; edit(π,X) ≡ break
#     38.   break; create(π,X) ≡ break
#     39.   break; remove(π) ≡ break
#     40.   edit(π, X); break ≡ break
#     41.   create(π, X); break ≡ break
#     42.   remove(π); break ≡ break
#     43.   break; break ≡ break
#
#     Remaining pairs
#     6F.   remove(π/π′); edit(π,File(m,x))
#     35.   create(π,X); create(π/π′,Y)
#     36.   remove(π/π′); remove(π)
#
#     Non-pair laws
#     Bottom. break ⊑ S for any S
#     Reflexivity. S ⊑ S for any S
#     S1 ⊑ S2 S2 ⊑ S3 (Transitivity) S1 ⊑ S3
#     S1 ⊑ S2 (Substitution) S;S1;S′ ⊑ S;S2;S′
#
#     N.B. Paths π and φ are always incomparable. Where we write π/π′, π′ is always nonempty.
#
# REMOVE INCOMPARABLE PATH LAWS, BREAK LAWS AND NON-PAIR LAWS
#
#     Commuting or approximating pairs
#     1.    edit(π,X); edit(π/π′,Y) ≡ edit(π/π′,Y); edit(π,X)
#     2.    edit(π/π′,Y); edit(π,X) ≡ edit(π,X); edit(π/π′,Y)
#     3D⊑.  edit(π,Dir(m)); create(π/π′,Y) ⊒ create(π/π′,Y); edit(π, Dir(m))
#     4D⊑.  create(π/π′,Y); edit(π,Dir(m)) ⊑ edit(π, Dir(m)); create(π/π′,Y)
#     5D.   edit(π, Dir(m)); remove(π/π′) ≡ remove(π/π′); edit(π, Dir(m))
#     6D.   remove(π/π′); edit(π, Dir(m)) ≡ edit(π, Dir(m)); remove(π/π′)
#
#     Incorrect pairs
#     3F.   edit(π, File(m, x)); create(π/π′,Y) ≡ break
#     4F.   create(π/π′,Y); edit(π, File(m, x)) ≡ break
#     5F.   edit(π, File(m, x)); remove(π/π′) ≡ break
#     16.   edit(π, X); create(π,Y) ≡ break
#     17.   edit(π/π′,X); create(π,Y) ≡ break
#     18.   edit(π/π′,X); remove(π) ≡ break
#     19.   create(π,X); edit(π/π′,Y) ≡ break
#     20.   create(π,X); create(π,Y ) ≡ break
#     21.   create(π/π′,X); create(π,Y) ≡ break
#     22.   create(π,X); remove(π/π′) ≡ break
#     23.   create(π/π′,X); remove(π) ≡ break
#     24.   remove(π); edit(π,X) ≡ break
#     25.   remove(π); edit(π/π′,X) ≡ break
#     26.   remove(π); create(π/π′,X) ≡ break
#     27.   remove(π/π′); create(π,X) ≡ break
#     28.   remove(π); remove(π) ≡ break
#     29.   remove(π); remove(π/π′) ≡ break
#
#     Simplifying laws
#     30⊑.  edit(π,X); edit(π,Y) ⊑ edit(π,Y)
#     31.   edit(π,X); remove(π) ≡ remove(π)
#     32.   create(π,X); edit(π,Y) ≡ create(π,Y)
#     33⊑.  create(π,X); remove(π) ⊑ skip
#     34⊑.  remove(π); create(π,X) ⊑ edit(π,X)
#
#     Remaining pairs
#     6F.   remove(π/π′); edit(π,File(m,x))
#     35.   create(π,X); create(π/π′,Y)
#     36.   remove(π/π′); remove(π)
#
# REMOVE ALL REMOVE() OPERATIONS BECAUSE REMOVE() OPERATIONS ARE IGNORED
# WHEN CONFLICTS EXIST
#
#     Commuting or approximating pairs
#     1.    edit(π,X); edit(π/π′,Y) ≡ edit(π/π′,Y); edit(π,X)
#     2.    edit(π/π′,Y); edit(π,X) ≡ edit(π,X); edit(π/π′,Y)
#     3D⊑.  edit(π,Dir(m)); create(π/π′,Y) ⊒ create(π/π′,Y); edit(π, Dir(m))
#     4D⊑.  create(π/π′,Y); edit(π,Dir(m)) ⊑ edit(π, Dir(m)); create(π/π′,Y)
#
#     Incorrect pairs
#     3F.   edit(π, File(m, x)); create(π/π′,Y) ≡ break
#     4F.   create(π/π′,Y); edit(π, File(m, x)) ≡ break
#     16.   edit(π, X); create(π,Y) ≡ break
#     17.   edit(π/π′,X); create(π,Y) ≡ break
#     19.   create(π,X); edit(π/π′,Y) ≡ break
#     20.   create(π,X); create(π,Y ) ≡ break
#     21.   create(π/π′,X); create(π,Y) ≡ break
#
#     Simplifying laws
#     30⊑.  edit(π,X); edit(π,Y) ⊑ edit(π,Y)
#     32.   create(π,X); edit(π,Y) ≡ create(π,Y)
#
#     Remaining pairs
#     35.   create(π,X); create(π/π′,Y)
#
# SORT OPERATIONS AND KEEP ONLY PAIRS
# REMOVE ⊑ MARKER
#
#     20.   create(π,X) - create(π,Y)
#     35.   create(π,X) - create(π/π′,Y)
#     21.   create(π/π′,X) - create(π,Y)
#     32.   create(π,X) - edit(π,Y)
#     19.   create(π,X) - edit(π/π′,Y)
#     4F.   create(π/π′,Y) - edit(π,File(m,x))
#     4D.   create(π/π′,Y) - edit(π,Dir(m))
#     30.   edit(π,X) - edit(π,Y)
#     1.    edit(π,X) - edit(π/π′,Y)
#     2.    edit(π/π′,Y) - edit(π,X)
#     3F.   edit(π,File(m,x)) - create(π/π′,Y)
#     3D.   edit(π,Dir(m)) - create(π/π′,Y)
#     16.   edit(π,X) - create(π,Y)
#     17.   edit(π/π′,X) - create(π,Y)
#
# REMOVE PAIRS 32, 19, 16, 17 BECAUSE THEY ARE NOT POSSIBLE
# WHEN DIFF'ING TWO REPLICAS AGAINST THE SAME SNAPSHOT
#
#     20.   create(π,X) - create(π,Y)
#     35.   create(π,X) - create(π/π′,Y)
#     21.   create(π/π′,X) - create(π,Y)
#     4F.   create(π/π′,Y) - edit(π,File(m,x))
#     4D.   create(π/π′,Y) - edit(π,Dir(m))
#     30.   edit(π,X) - edit(π,Y)
#     1.    edit(π,X) - edit(π/π′,Y)
#     2.    edit(π/π′,Y) - edit(π,X)
#     3F.   edit(π,File(m,x)) - create(π/π′,Y)
#     3D.   edit(π,Dir(m)) - create(π/π′,Y)
#
# REMOVE PAIRS 35, 21 BECAUSE PAIR 20 WILL ALSO EXIST
# AND THEY WILL BE RECONCILED BY IT
#
#     20.   create(π,X) - create(π,Y)
#     4F.   create(π/π′,Y) - edit(π,File(m,x))
#     4D.   create(π/π′,Y) - edit(π,Dir(m))
#     30.   edit(π,X) - edit(π,Y)
#     1.    edit(π,X) - edit(π/π′,Y)
#     2.    edit(π/π′,Y) - edit(π,X)
#     3F.   edit(π,File(m,x)) - create(π/π′,Y)
#     3D.   edit(π,Dir(m)) - create(π/π′,Y)
#
# REWRITE Y on LEFT, X on RIGHT
# REWRITE ALL PAIRS AS F AND D VARIANTS
#
#     20FF. create(π,Y=File(m,y)) - create(π,X=File(n,x))
#     20FD. create(π,Y=File(m,y)) - create(π,X=Dir(n))
#     20DF. create(π,Y=Dir(m)) - create(π,X=File(n,x))
#     20DD. create(π,Y=Dir(m)) - create(π,X=Dir(n))
#     4F.   create(π/π′,Y) - edit(π,X=File(m,x))
#     4D.   create(π/π′,Y) - edit(π,X=Dir(m))
#     30FF. edit(π,Y=File(m,y)) - edit(π,X=File(n,x))
#     30FD. edit(π,Y=File(m,y)) - edit(π,X=Dir(n))
#     30DF. edit(π,Y=Dir(m)) - edit(π,X=File(n,x))
#     30DD. edit(π,Y=Dir(m)) - edit(π,X=Dir(n))
#     1F.   edit(π,Y=File(m,y)) - edit(π/π′,X)
#     1D.   edit(π,Y=Dir(m)) - edit(π/π′,X)
#     2F.   edit(π/π′,Y) - edit(π,X=File(m,x))
#     2D.   edit(π/π′,Y) - edit(π,X=Dir(m))
#     3F.   edit(π,Y=File(m,y)) - create(π/π′,X)
#     3D.   edit(π,Y=Dir(m)) - create(π/π′,X)
#
# REMOVE PAIRS 1D, 2D, 3D, 4D BECAUSE THEY COMMUTE AND
# BECAUSE THERE CAN BE NO CONFLICT IN PAIRS THAT PRECEDE THEM
#
#     20FF. create(π,Y=File(m,y)) - create(π,X=File(n,x))
#     20FD. create(π,Y=File(m,y)) - create(π,X=Dir(n))
#     20DF. create(π,Y=Dir(m)) - create(π,X=File(n,x))
#     20DD. create(π,Y=Dir(m)) - create(π,X=Dir(n))
#     4F.   create(π/π′,Y) - edit(π,X=File(m,x))
#     30FF. edit(π,Y=File(m,y)) - edit(π,X=File(n,x))
#     30FD. edit(π,Y=File(m,y)) - edit(π,X=Dir(n))
#     30DF. edit(π,Y=Dir(m)) - edit(π,X=File(n,x))
#     30DD. edit(π,Y=Dir(m)) - edit(π,X=Dir(n))
#     1F.   edit(π,Y=File(m,y)) - edit(π/π′,X)
#     2F.   edit(π/π′,Y) - edit(π,X=File(m,x))
#     3F.   edit(π,Y=File(m,y)) - create(π/π′,X)
#
# RECONCILE REMAINING PAIRS
#
#     20FF. create(π,Y=File(m,y)) - create(π,X=File(n,x))
#           rename(π,unique₁(π)); create(unique₂(π),Y) - rename(π,unique₂(π)); create(unique₁(π),X)
#     20FD. create(π,Y=File(m,y)) - create(π,X=Dir(n))
#           create(unique₂(π),Y) - rename(π,unique₂(π)); create(π,X)
#     20DF. create(π,Y=Dir(m)) - create(π,X=File(n,x))
#           rename(π,unique₁(π)); create(π,Y) - create(unique₁(π),X)
#     20DD. create(π,Y=Dir(m)) - create(π,X=Dir(n))
#           meta_reconcile(π) - meta_reconcile(π)
#     4F.   create(π/π′,Y) - edit(π,X=File(m,x))
#           rename(π,unique₁(π)); create(π,F₂(π)); create(π/π′,Y) - create(unique₁(π),X)
#     30FF. edit(π,Y=File(m,y)) - edit(π,X=File(n,x))
#           rename(π,unique₁(π)); create(unique₂(π),Y) - rename(π,unique₂(π)); create(unique₁(π),X)
#     30FD. edit(π,Y=File(m,y)) - edit(π,X=Dir(n))
#           create(unique₂(π),Y) - rename(π,unique₂(π)); create(π,X)
#     30DF. edit(π,Y=Dir(m)) - edit(π,X=File(n,x))
#           rename(π,unique₁(π)); create(π,Y) - create(unique₁(π),X)
#     30DD. edit(π,Y=Dir(m)) - edit(π,X=Dir(n))
#           meta_reconcile(π) - meta_reconcile(π)
#     1F.   edit(π,Y=File(m,y)) - edit(π/π′,X)
#           create(unique₂(π),Y) - rename(π,unique₂(π)); create(π,F₁(π)); edit(π/π′,X)
#     2F.   edit(π/π′,Y) - edit(π,X=File(m,x))
#           rename(π,unique₁(π)); create(π,F₂(π)); edit(π/π′,Y) - create(unique₁(π),X)
#     3F.   edit(π,Y=File(m,y)) - create(π/π′,X)
#           create(unique₂(π),Y) - rename(π,unique₂(π)); create(π,F₁(π)); create(π/π′,X)

import copy, hashlib, os, shutil, stat
from datetime import datetime

class Namespace:
    def __init__(self, **kwds):
        self.__dict__.update(kwds)

def meta_equals(m1, m2):
    return m1.st_mode == m2.st_mode
def meta_isdir(m):
    return stat.S_ISDIR(m.st_mode)
def meta_reconcile(m1, m2):
    m = Namespace()
    m.st_mode = m1.st_mode & m2.st_mode
    return m

def path_isprefix(path1, path2):
    p1 = path1.split(os.sep)
    p2 = path2.split(os.sep)
    return len(p1) <= len(p2) and p1 == p2[:len(p1)]

class Content(object):
    def __init__(self, meta, path):
        self.meta = meta
        self.path = path
    def __eq__(self, other):
        return meta_equals(self.meta, other.meta) and \
            (meta_isdir(self.meta) or self.path == other.path)
    def __str__(self):
        return "%#04o,%s" % (self.meta.st_mode, self.path)

class Operation(object):
    CREATE = "C"
    REMOVE = "R"
    UPDATE = "U"
    UPDDIR = "D"
    RENUNQ = "Q"                        # special
    __kind_syms = {
        "C": "CREATE",
        "R": "REMOVE",
        "U": "UPDATE",
        "D": "UPDDIR",
        "r": "RENUNQ",
    }
    __commute_laws = [
        ("u", 0, "u", 1),               # 1
        ("u", 1, "u", 0),               # 2
        ("D", 0, "C", 1),               # 3D ???
        ("C", 1, "D", 0),               # 4D ???
        ("D", 0, "R", 1),               # 5D
        ("R", 1, "D", 0),               # 6D
    ]
    __break_laws = [
        ("U", 0, "u", 1),               # 1F
        ("u", 1, "U", 0),               # 2F
        ("U", 0, "C", 1),               # 3F
        ("C", 1, "U", 0),               # 4F
        ("U", 0, "R", 1),               # 5F
        ("u", 0, "C", 0),               # 16
        ("u", 1, "C", 0),               # 17
        ("u", 1, "R", 0),               # 18
        ("C", 0, "u", 1),               # 19
        ("C", 0, "C", 0),               # 20
        ("C", 1, "C", 0),               # 21
        ("C", 0, "R", 1),               # 22
        ("C", 1, "R", 0),               # 23
        ("R", 0, "u", 0),               # 24
        ("R", 0, "u", 1),               # 25
        ("R", 0, "C", 1),               # 26
        ("R", 1, "C", 0),               # 27
        ("R", 0, "R", 0),               # 28
        ("R", 0, "R", 1),               # 29
    ]
    __reconcile_laws = [
        ("C", 0x10, "C", 0x10),         # 20FF
        ("U", 0x10, "U", 0x10),         # 30FF
        ("C", 0x10, "C", 0x20),         # 20FD
        ("U", 0x10, "D", 0x20),         # 30FD
        ("C", 0x20, "C", 0x10),         # 20DF
        ("D", 0x20, "U", 0x10),         # 30DF
        ("U", 0x10, "u", 0x01),         # 1F
        ("U", 0x10, "C", 0x01),         # 3F
        ("C", 0x01, "U", 0x10),         # 4F
        ("u", 0x01, "U", 0x10),         # 2F
        ("C", 0x20, "C", 0x20),         # 20DD
        ("D", 0x20, "D", 0x20),         # 30DD
    ]
    __reconcile_names = [
        "20FF",
        "30FF",
        "20FD",
        "30FD",
        "20DF",
        "30DF",
        "1F",
        "3F",
        "4F",
        "2F",
        "20DD",
        "30DD",
    ]
    def __init__(self, kind, path, content, metaonly = False):
        self.kind = kind
        self.path = os.path.normpath(path)
        self.content = content
        self.metaonly = metaonly
        self.ignore = False
    def __eq__(self, other):
        return self.kind == other.kind and self.path == other.path and \
            self.content == other.content and self.metaonly == other.metaonly
    def __str__(self):
        return "%s%s %s content=%s%s" % (
            "#" if self.ignore else "", Operation.__kind_syms[self.kind], self.path,
            "(M)" if self.metaonly else "", self.content)
    def __match(self, other, laws, metaalso):
        if self.path == other.path:
            sp, op = 0, 0
        elif path_isprefix(self.path, other.path):
            sp, op = 0, 1
        elif path_isprefix(other.path, self.path):
            sp, op = 1, 0
        else:
            return -1                   # incomparable paths
        if metaalso:
            sp |= 0x20 if meta_isdir(self.content.meta) else 0x10
            op |= 0x20 if meta_isdir(other.content.meta) else 0x10
        for i in xrange(len(laws)):
            law = laws[i]
            l0 = [law[0]] if law[0] != "u" else ["U", "D"]
            l1 = [law[1]] if (law[1] & 0xf0) != 0 else [law[1], 0x10 | law[1], 0x20 | law[1]]
            l2 = [law[2]] if law[2] != "u" else ["U", "D"]
            l3 = [law[3]] if (law[3] & 0xf0) != 0 else [law[3], 0x10 | law[3], 0x20 | law[3]]
            if self.kind in l0 and sp in l1 and other.kind in l2 and op in l3:
                return i + 1            # match found
        return 0                        # no match found
    def commutes(self, other):
        return 0 != self.__match(other, Operation.__commute_laws, False)
    def breaks(self, other):
        return 0 <  self.__match(other, Operation.__break_laws, False)
    def conflicts(self, other):
        return not self.commutes(other) or self.breaks(other) or other.breaks(self)
    def reconcile(self, other):
        i = self.__match(other, Operation.__reconcile_laws, True)
        if 0 < i:
            return Operation.__reconcile_names[i - 1]
        else:
            return None
    def copy(self):
        return copy.copy(self)

class FilesystemError(Exception):
    pass
class Filesystem(object):
    def __init__(self, name, path):
        self.name = name
        self.path = path
    def __str__(self):
        return "<%s %s>" % (type(self).__name__, self.path)
    def fsexists(self):
        raise NotImplementedError
    def fscreate(self):
        raise NotImplementedError
    def meta(self, path):
        raise NotImplementedError
    def listdir(self, path):
        raise NotImplementedError
    def digest(self, path):
        raise NotImplementedError
    def content(self, m, path):
        raise NotImplementedError
    def create(self, path, content):
        raise NotImplementedError
    def remove(self, path):
        raise NotImplementedError
    def update(self, path, content):
        raise NotImplementedError
    def upddir(self, path, content):
        raise NotImplementedError
    def rename(self, oldpath, newpath):
        raise NotImplementedError
    def perform(self, op):
        if op.kind == Operation.CREATE:
            return self.create(op.path, op.content)
        elif op.kind == Operation.REMOVE:
            return self.remove(op.path)
        elif op.kind == Operation.UPDATE:
            return self.update(op.path, op.content, op.metaonly)
        elif op.kind == Operation.UPDDIR:
            return self.upddir(op.path, op.content)
        else:
            raise ValueError

class FileFilesystem(Filesystem):
    def _copymeta(self, content, path):
        os.chmod(path, content.meta.st_mode)
    def _copyfile(self, content, path):
        return shutil.copyfile(content.path, path)
    def _digest(self, path):
        h = hashlib.sha256()
        with open(path, "rb") as f:
            while True:
                data = f.read(8192)
                if not data:
                    break
                h.update(data)
        return h.hexdigest()
    def fsexists(self):
        return os.path.exists(self.path)
    def fscreate(self):
        return os.makedirs(self.path)
    def meta(self, path):
        path = os.path.join(self.path, path)
        try:
            return os.lstat(path)
        except EnvironmentError:
            return None
    def listdir(self, path):
        path = os.path.join(self.path, path)
        if os.path.isdir(path):
            return os.listdir(path)
        else:
            return []
    def digest(self, path):
        path = os.path.join(self.path, path)
        return self._digest(path)
    def content(self, m, path):
        if path is not None:
            path = os.path.join(self.path, path)
        return Content(m, path)
    def create(self, path, content):
        path = os.path.join(self.path, path)
        if os.path.exists(path):
            raise FilesystemError("create: os.path.exists(%s)" % path)
        if meta_isdir(content.meta):
            os.mkdir(path)
        else:
            self._copyfile(content, path)
        self._copymeta(content, path)
    def remove(self, path):
        path = os.path.join(self.path, path)
        if os.path.isdir(path):
            os.rmdir(path)
        else:
            os.unlink(path)
    def update(self, path, content, metaonly):
        path = os.path.join(self.path, path)
        if meta_isdir(content.meta):
            raise FilesystemError
        if os.path.isdir(path):
            os.rmdir(path)
        if not metaonly:
            self._copyfile(content, path)
        self._copymeta(content, path)
    def upddir(self, path, content):
        path = os.path.join(self.path, path)
        if not os.path.exists(path):
            raise FilesystemError("upddir: not os.path.exists(%s)" % path)
        if not meta_isdir(content.meta):
            raise FilesystemError
        if not os.path.isdir(path):
            os.unlink(path); os.mkdir(path)
        self._copymeta(content, path)
    def rename(self, oldpath, newpath):
        oldpath = os.path.join(self.path, oldpath)
        newpath = os.path.join(self.path, newpath)
        if os.path.exists(newpath):
            raise FilesystemError("rename: os.path.exists(%s)" % newpath)
        os.rename(oldpath, newpath)
class SnapFilesystem(FileFilesystem):
    def _copyfile(self, content, path):
        with open(path, "wb") as f:
            f.write(self._digest(content.path))
    def digest(self, path):
        path = os.path.join(self.path, path)
        with open(path, "rb") as f:
            return f.read()

class Devnull(object):
    def write(self, *args):
        pass
devnull = Devnull()
class FilesystemSynchronizer(object):
    def __init__(self, snap, local, other):
        self.logfile = devnull
        self.verbose = 0
        self.dry_run = False
        self.ident = datetime.utcnow().strftime("%Y%m%dT%H%M%S")
        self.snapfs = snap
        self.localfs = local
        self.otherfs = other
    def __log(self, s):
        self.logfile.write(str(s) + "\n")
    def __log_sq(self, mesg, sq):
        flag = False
        for op in sq:
            if not flag:
                self.__log(mesg)
                flag = True
            self.__log("    " + str(op))
    def __log_sq_conflicted(self, mesg, sq_conflicted):
        flag = False
        for op1, op2 in sq_conflicted:
            if not flag:
                self.__log(mesg)
                flag = True
            self.__log("    " + str(op1))
            self.__log("        !!! " + str(op2))
    def __uniquepath(self, fsname, path):
        p = os.path.splitext(path)
        return "".join([p[0], "-%s-%s" % (fsname, self.ident), p[1]])
    def __listdir(self, fs1, fs2, path):
        l1 = fs1.listdir(path)
        l2 = fs2.listdir(path)
        return sorted(set(l1) | set(l2))
    def __fsdiff_op(self, fs1, fs2, path):
        m1 = fs1.meta(path)
        m2 = fs2.meta(path)
        if m1 is None:
            if m2 is None:
                return None
            else:
                return Operation(Operation.CREATE, path, fs2.content(m2, path), False)
        else:
            if m2 is None:
                return Operation(Operation.REMOVE, path, fs1.content(m1, None), False)
            elif not meta_isdir(m2):
                if meta_isdir(m1) or fs1.digest(path) != fs2.digest(path):
                    return Operation(Operation.UPDATE, path, fs2.content(m2, path), False)
                elif not meta_equals(m1, m2):
                    return Operation(Operation.UPDATE, path, fs2.content(m2, path), True)
                else:
                    return None
            else:
                if not meta_equals(m1, m2):
                    return Operation(Operation.UPDDIR, path, fs2.content(m2, path), False)
                else:
                    return None
    def __fsdiff_recurse(self, fs1, fs2, ops, path):
        op = self.__fsdiff_op(fs1, fs2, path)
        if op is not None and (op.kind == Operation.UPDDIR or op.kind == Operation.CREATE):
            ops[op.kind].append(op)
        for n in self.__listdir(fs1, fs2, path):
            self.__fsdiff_recurse(fs1, fs2, ops, os.path.join(path, n))
        if op is not None and (op.kind == Operation.REMOVE or op.kind == Operation.UPDATE):
            ops[op.kind].append(op)
    def __fsdiff(self, fs1, fs2):
        ops = {
            Operation.CREATE: [],
            Operation.REMOVE: [],
            Operation.UPDATE: [],
            Operation.UPDDIR: [],
        }
        self.__fsdiff_recurse(fs1, fs2, ops, "")
        return ops[Operation.UPDDIR] + ops[Operation.CREATE] + \
            ops[Operation.REMOVE] + ops[Operation.UPDATE]
    def __fsupdate0(self, fs, sq):
        for op in sq:
            if op.kind == Operation.RENUNQ and not op.ignore:
                newpath = self.__uniquepath(fs.name, op.path)
                if 1 <= self.verbose:
                    self.__log("fsupdate0 %s: rename %s %s" % (fs.name, op.path, newpath))
                fs.rename(op.path, newpath)
    def __fsupdate(self, fs, sq):
        for op in sq:
            if op.kind != Operation.RENUNQ and not op.ignore:
                if 1 <= self.verbose:
                    self.__log("fsupdate %s: %s" % (fs.name, op))
                fs.perform(op)
    def __conflicts(self, op, sq):
        return [o for o in sq if op.conflicts(o)]
    def __propagate(self, sq1, sq2, sq_conflicted, dir):
        sq_propagated = []
        for i in xrange(len(sq1)):
            op1 = sq1[i]
            if op1 in sq2:
                continue
            for op2 in self.__conflicts(op1, sq2):
                if not op2 in sq1:
                    if (op1, op2) not in sq_conflicted and (op2, op1) not in sq_conflicted:
                        sq_conflicted.append((op1, op2) if 0 < dir else (op2, op1))
            # for o in sq1[:i]:
            #     if not o.commutes(op1):
            #         for op2 in self.__conflicts(o, sq2):
            #             if not op2 in sq1:
            #                 if (op1, op2) not in sq_conflicted and (op2, op1) not in sq_conflicted:
            #                     sq_conflicted.append((op1, op2) if 0 < dir else (op2, op1))
            sq_propagated.append(op1)
        return sq_propagated
    def __reconcile(self, lsq_propagated, osq_propagated, sq_conflicted):
        lname = self.localfs.name
        oname = self.otherfs.name
        lsq_propaghead = []
        osq_propaghead = []
        for opo, opl in sq_conflicted:
            if opo.kind == Operation.REMOVE and not opo.ignore:
                if meta_isdir(opo.content.meta):
                    osq_propaghead.append(Operation(Operation.CREATE, opo.path, opo.content))
                elif opl.kind == Operation.UPDDIR:
                    assert opo.path == opl.path
                    osq_propaghead.append(Operation(Operation.CREATE, opl.path, opl.content))
                if opl.kind == Operation.UPDATE:
                    opl.metaonly = False
                opo.ignore = True
            if opl.kind == Operation.REMOVE and not opl.ignore:
                if meta_isdir(opl.content.meta):
                    lsq_propaghead.append(Operation(Operation.CREATE, opl.path, opl.content))
                elif opo.kind == Operation.UPDDIR:
                    assert opo.path == opl.path
                    lsq_propaghead.append(Operation(Operation.CREATE, opo.path, opo.content))
                if opo.kind == Operation.UPDATE:
                    opo.metaonly = False
                opl.ignore = True
            if opo.ignore or opl.ignore:
                continue
            pair = opo.reconcile(opl)
            if pair == "20FF" or pair == "30FF":
#     20FF. create(π,Y=File(m,y)) - create(π,X=File(n,x))
#           rename(π,unique₁(π)); create(unique₂(π),Y) - rename(π,unique₂(π)); create(unique₁(π),X)
#     30FF. edit(π,Y=File(m,y)) - edit(π,X=File(n,x))
#           rename(π,unique₁(π)); create(unique₂(π),Y) - rename(π,unique₂(π)); create(unique₁(π),X)
                opo.ignore = opl.ignore = True
                lsq_propagated.append(Operation(Operation.RENUNQ, opo.path, None))
                lsq_propagated.append(Operation(Operation.CREATE,
                    self.__uniquepath(oname, opo.path),
                    self.otherfs.content(opo.content.meta,
                        self.__uniquepath(oname, opo.content.path)), False))
                osq_propagated.append(Operation(Operation.RENUNQ, opl.path, None))
                osq_propagated.append(Operation(Operation.CREATE,
                    self.__uniquepath(lname, opl.path),
                    self.localfs.content(opl.content.meta,
                        self.__uniquepath(lname, opl.content.path)), False))
            elif pair == "20FD" or pair == "30FD":
#     20FD. create(π,Y=File(m,y)) - create(π,X=Dir(n))
#           create(unique₂(π),Y) - rename(π,unique₂(π)); create(π,X)
#     30FD. edit(π,Y=File(m,y)) - edit(π,X=Dir(n))
#           create(unique₂(π),Y) - rename(π,unique₂(π)); create(π,X)
                opo.ignore = opl.ignore = True
                lsq_propagated.append(Operation(Operation.CREATE,
                    self.__uniquepath(oname, opo.path),
                    self.otherfs.content(opo.content.meta,
                        self.__uniquepath(oname, opo.content.path)), False))
                osq_propagated.append(Operation(Operation.RENUNQ, opl.path, None))
                osq_propaghead.append(Operation(Operation.CREATE, opl.path, opl.content))
            elif pair == "20DF" or pair == "30DF":
#     20DF. create(π,Y=Dir(m)) - create(π,X=File(n,x))
#           rename(π,unique₁(π)); create(π,Y) - create(unique₁(π),X)
#     30DF. edit(π,Y=Dir(m)) - edit(π,X=File(n,x))
#           rename(π,unique₁(π)); create(π,Y) - create(unique₁(π),X)
                opo.ignore = opl.ignore = True
                lsq_propagated.append(Operation(Operation.RENUNQ, opo.path, None))
                lsq_propaghead.append(Operation(Operation.CREATE, opo.path, opo.content))
                osq_propagated.append(Operation(Operation.CREATE,
                    self.__uniquepath(lname, opl.path),
                    self.localfs.content(opl.content.meta,
                        self.__uniquepath(lname, opl.content.path)), False))
            elif pair == "1F" or pair == "3F":
#     1F.   edit(π,Y=File(m,y)) - edit(π/π′,X)
#           create(unique₂(π),Y) - rename(π,unique₂(π)); create(π,F₁(π)); edit(π/π′,X)
#     3F.   edit(π,Y=File(m,y)) - create(π/π′,X)
#           create(unique₂(π),Y) - rename(π,unique₂(π)); create(π,F₁(π)); create(π/π′,X)
                opo.ignore = True
                lsq_propagated.append(Operation(Operation.CREATE,
                    self.__uniquepath(oname, opo.path),
                    self.otherfs.content(opo.content.meta,
                        self.__uniquepath(oname, opo.content.path)), False))
                osq_propagated.append(Operation(Operation.RENUNQ, opo.path, None))
                osq_propaghead.append(Operation(Operation.CREATE, opo.path,
                    self.otherfs.content(self.localfs.meta(opo.path), opo.path), False))
            elif pair == "4F" or pair == "2F":
#     4F.   create(π/π′,Y) - edit(π,X=File(m,x))
#           rename(π,unique₁(π)); create(π,F₂(π)); create(π/π′,Y) - create(unique₁(π),X)
#     2F.   edit(π/π′,Y) - edit(π,X=File(m,x))
#           rename(π,unique₁(π)); create(π,F₂(π)); edit(π/π′,Y) - create(unique₁(π),X)
                opl.ignore = True
                lsq_propagated.append(Operation(Operation.RENUNQ, opl.path, None))
                lsq_propaghead.append(Operation(Operation.CREATE, opl.path,
                    self.localfs.content(self.otherfs.meta(opl.path), opl.path), False))
                osq_propagated.append(Operation(Operation.CREATE,
                    self.__uniquepath(lname, opl.path),
                    self.localfs.content(opl.content.meta,
                        self.__uniquepath(lname, opl.content.path)), False))
#     20DD. create(π,Y=Dir(m)) - create(π,X=Dir(n))
#           meta_reconcile(π) - meta_reconcile(π)
            elif pair == "20DD":
                opo.ignore = opl.ignore = True
                opo.content.meta = meta_reconcile(opo.content.meta, opl.content.meta)
                opl.content.meta = meta_reconcile(opo.content.meta, opl.content.meta)
                lsq_propagated.append(Operation(Operation.UPDDIR, opo.path, opo.content))
                osq_propagated.append(Operation(Operation.UPDDIR, opl.path, opl.content))
            elif pair == "30DD":
#     30DD. edit(π,Y=Dir(m)) - edit(π,X=Dir(n))
#           meta_reconcile(π) - meta_reconcile(π)
                opo.content.meta = meta_reconcile(opo.content.meta, opl.content.meta)
                opl.content.meta = meta_reconcile(opo.content.meta, opl.content.meta)
            else:
                print pair, opo, opl
                assert False
        lsq_propaghead.sort(key=lambda op: op.path)
        osq_propaghead.sort(key=lambda op: op.path)
        lsq_propagated[0:0] = lsq_propaghead
        osq_propagated[0:0] = osq_propaghead
    def synchronize(self):
        if not self.snapfs.fsexists():
            if 1 <= self.verbose:
                self.__log("fscreate %s" % self.snapfs)
            if not self.dry_run:
                self.snapfs.fscreate()
        lsq = self.__fsdiff(self.snapfs, self.localfs)
        osq = self.__fsdiff(self.snapfs, self.otherfs)
        ssq = [op.copy() for op in lsq]
        if 2 <= self.verbose:
            self.__log_sq("lsq", lsq)
            self.__log_sq("osq", osq)
        sq_conflicted = []
        lsq_propagated = self.__propagate(osq, lsq, sq_conflicted, +1)
        osq_propagated = self.__propagate(lsq, osq, sq_conflicted, -1)
        if 2 <= self.verbose:
            self.__log_sq("lsq_propagated", lsq_propagated)
            self.__log_sq("osq_propagated", osq_propagated)
            self.__log_sq_conflicted("sq_conflicted", sq_conflicted)
        self.__reconcile(lsq_propagated, osq_propagated, sq_conflicted)
        if not self.dry_run:
            self.__fsupdate(self.snapfs, ssq)
            self.__fsupdate0(self.snapfs, lsq_propagated)
            self.__fsupdate0(self.localfs, lsq_propagated)
            self.__fsupdate0(self.otherfs, osq_propagated)
            self.__fsupdate(self.snapfs, lsq_propagated)
            self.__fsupdate(self.localfs, lsq_propagated)
            self.__fsupdate(self.otherfs, osq_propagated)

if "__main__" == __name__:
    import argparse, sys
    def info(s):
        print "%s: %s" % (os.path.basename(sys.argv[0]), s)
    def warn(s):
        print >> sys.stderr, "%s: %s" % (os.path.basename(sys.argv[0]), s)
    def fail(s, exitcode = 1):
        warn(s)
        sys.exit(exitcode)
    def main():
        p = argparse.ArgumentParser()
        p.add_argument("-v", "--verbose", action="count", default=0, help="verbosity level")
        p.add_argument("-n", "--dry-run", action="store_true",
            help="compute and print updates, do not perform them (implies -v)")
        p.add_argument("-i", "--ident", help="identifier for conflict resolution")
        p.add_argument("snapfs", help="snapshot file system")
        p.add_argument("localfs", help="local file system")
        p.add_argument("otherfs", help="other/remote file system")
        args = p.parse_args(sys.argv[1:])
        if args.dry_run and not args.verbose:
            args.verbose = 1
        args.snapfs = os.path.realpath(args.snapfs)
        args.localfs = os.path.realpath(args.localfs)
        args.otherfs = os.path.realpath(args.otherfs)
        if args.snapfs == args.localfs or args.snapfs == args.otherfs or args.localfs == args.otherfs:
            fail("file system paths must not be the same")
        if os.path.exists(args.snapfs) and not os.path.isdir(args.snapfs):
            fail("snapshot file system must not exist or be a directory")
        if not os.path.isdir(args.localfs) or not os.path.isdir(args.otherfs):
            fail("local and other file systems must exist and be directories")
        fssync = FilesystemSynchronizer(SnapFilesystem("LOCAL", args.snapfs),
            FileFilesystem("LOCAL", args.localfs), FileFilesystem("OTHER", args.otherfs))
        fssync.logfile = sys.stderr
        fssync.verbose = args.verbose
        fssync.dry_run = args.dry_run
        if args.ident:
            fssync.ident = args.ident
        fssync.synchronize()
    def __entry():
        try:
            main()
        except KeyboardInterrupt:
            fail("interrupted", 130)
    __entry()
