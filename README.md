# File System Synchronization Utility #

This repository contains a utility called fssync.py and the corresponding test suite. The utility implements a file synchronization algorithm that is based on the paper "An Algebraic Approach to File Synchronization" by Norman Ramsey and Elod Csirmaz. An automatic reconciliation technique has been added beyond what is directly described in the paper.

The utility fssync.py can be used either as a Python module or a command line utility. The test suite requires the fsrand.py and cmptree.py utilities, which are part of the secfs.test repository found at https://bitbucket.org/billziss/secfs.test

Released under the BSD 3-clause license.